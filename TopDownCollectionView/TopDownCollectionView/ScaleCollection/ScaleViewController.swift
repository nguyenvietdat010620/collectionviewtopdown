//
//  ScaleViewController.swift
//  TopDownCollectionView
//
//  Created by Nguyen Viet Dat on 06/12/2023.
//

import UIKit

class ScaleViewController: UIViewController {
    
    @IBOutlet weak var scaleCollection: UICollectionView!
    var listData = [1,2,3,4,5,6,7,8,9,10]
    override func viewDidLoad() {
        super.viewDidLoad()

        scaleCollection.delegate = self
        scaleCollection.dataSource = self
        scaleCollection.backgroundColor = .red
        let nib = UINib(nibName: "ScaleCollectionViewCell", bundle: nil)
        scaleCollection.register(nib, forCellWithReuseIdentifier: "ScaleCollectionViewCell")
        
//        let customLayout = ScaleCollection()
//        scaleCollection.collectionViewLayout = customLayout
        
        
        
        // Do any additional setup after loading the view.
    }
    
    func getImageSize() {
        let sourceImage = UIImage(
            named: "imageNameInBundle"
        )!

        // The shortest side
        let sideLength = min(
            sourceImage.size.width,
            sourceImage.size.height
        )
        let sourceSize = sourceImage.size
        let xOffset = (sourceSize.width - sideLength) / 2.0
        let yOffset = (sourceSize.height - sideLength) / 2.0
        let cropRect = CGRect(
            x: xOffset,
            y: yOffset,
            width: sideLength,
            height: sideLength
        ).integral

        // Center crop the image
        let sourceCGImage = sourceImage.cgImage!
        let croppedCGImage = sourceCGImage.cropping(
            to: cropRect
        )!
    }

}

extension ScaleViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ScaleCollectionViewCell", for: indexPath as IndexPath) as! ScaleCollectionViewCell
        cell.viewScale.backgroundColor = UIColor.random
        return cell
    }
    
}
extension ScaleViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
}
extension UIImage {

    var getWidth: CGFloat {
        get {
            let width = self.size.width
            return width
        }
    }

    var getHeight: CGFloat {
        get {
            let height = self.size.height
            return height
        }
    }
}
