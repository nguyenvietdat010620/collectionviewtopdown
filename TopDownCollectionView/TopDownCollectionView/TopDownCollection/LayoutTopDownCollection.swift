//
//  LayoutTopDownCollection.swift
//  TopDownCollectionView
//
//  Created by Nguyen Viet Dat on 06/12/2023.
//

import Foundation
import UIKit

class LayoutTopDownCollection: UICollectionViewLayout {
    var numberColum = 3
    var contentSize:CGSize!
    let attributeArray = NSMutableDictionary()
    var space = 5
    let screenSize: CGRect = UIScreen.main.bounds
    var delegateCustom: ProtocolCustom?
    var data: [Int] = []
    
//    override init() {
//        super.init()
//        data = (delegateCustom?.getColumData())!
//    }
//    
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    var numberItem: Int{
        return (collectionView?.numberOfItems(inSection: 0))!
    }
    var itemHeight: CGFloat{
        return 50
    }
    var itemWidth:CGFloat{
        return (screenSize.width - CGFloat(numberColum * space)) / 3
    }
    
    override func prepare() {
        let minIteminColum = numberItem / numberColum
        let maxIteminColum = numberItem / numberColum + 1
        for i in 0 ... numberItem - 1 {
            var tempX : CGFloat = 0.0
            var tempY : CGFloat = 0.0
            let indexPath = NSIndexPath(item: i, section: 0)
            
            if(i < maxIteminColum){
                tempX = 0.0
                tempY = itemHeight * CGFloat(i) + CGFloat(space * i)
            }
            else if (i < (numberItem - minIteminColum)){ // 67
                tempX = (itemWidth + CGFloat(space))
                tempY = itemHeight * CGFloat((i - maxIteminColum)) + CGFloat(space * (i -  maxIteminColum))
            }
            else {
                tempX = (itemWidth * 2 + CGFloat(space * 2))
                tempY = itemHeight * CGFloat((i - (maxIteminColum + minIteminColum))) + CGFloat(space  * (i -  (maxIteminColum + minIteminColum)))
            }
            let attributes = UICollectionViewLayoutAttributes(forCellWith:indexPath as IndexPath);
            attributes.frame = CGRect(x: tempX, y: tempY, width: itemWidth, height: itemHeight);
            self.attributeArray.setObject(attributes, forKey: indexPath)
        }
        let contentHeight = CGFloat(maxIteminColum) * itemHeight + CGFloat(maxIteminColum * space)
        
        self.contentSize = CGSize(width: screenSize.width, height: contentHeight);
    }
    
    override var collectionViewContentSize: CGSize
    {
        return self.contentSize
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        
        // Duyệt các đối tượng trong attributeArray để tìm ra các cell nằm trong khung nhìn rect
        for attributes  in self.attributeArray {
            if (attributes.value as! UICollectionViewLayoutAttributes).frame.intersects(rect ) {
                layoutAttributes.append((attributes.value as! UICollectionViewLayoutAttributes))
            }
        }
        return layoutAttributes
    }
    
}



