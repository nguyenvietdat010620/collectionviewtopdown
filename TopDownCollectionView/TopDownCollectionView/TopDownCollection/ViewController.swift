//
//  ViewController.swift
//  TopDownCollectionView
//
//  Created by Nguyen Viet Dat on 06/12/2023.
//

import UIKit

protocol ProtocolCustom {
    func getColumData() -> [Int]
}

class ViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var listData = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "TopDownCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "TopDownCollectionViewCell")
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.collectionViewLayout = LayoutTopDownCollection()
        
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
//        collectionView.addGestureRecognizer(tapGestureRecognizer)
        // Do any additional setup after loading the view.
    }

//    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
//    {
//        switch tapGestureRecognizer.state{
//        
//        
//        case .began:
//            guard let indexPath = collectionView.indexPathForItem(at: tapGestureRecognizer.location(in: collectionView))
//            else{
//                return
//            }
//            collectionView.beginInteractiveMovementForItem(at: indexPath)
//        case .changed:
//            collectionView.updateInteractiveMovementTargetPosition(tapGestureRecognizer.location(in:collectionView))
//        case .ended:
//            collectionView.endInteractiveMovement()
//        default:
//            collectionView.cancelInteractiveMovement()
//        }
//        // Your action
//    }

}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopDownCollectionViewCell", for: indexPath as IndexPath) as! TopDownCollectionViewCell
        cell.labelCell.text = "\(indexPath)"
        cell.viewCell.backgroundColor = UIColor.random
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print(indexPath)
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
//        return true
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
//        let item = listData.remove(at: sourceIndexPath.row)
//        listData.insert(item, at: destinationIndexPath.row)
//    }
}


extension UIColor {
    static var random: UIColor {
        return UIColor(
            red: .random(in: 0...1),
            green: .random(in: 0...1),
            blue: .random(in: 0...1),
            alpha: 1.0
        )
    }
}
