//
//  TopDownCollectionViewCell.swift
//  TopDownCollectionView
//
//  Created by Nguyen Viet Dat on 06/12/2023.
//

import UIKit

class TopDownCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelCell: UILabel!
    @IBOutlet weak var viewCell: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
