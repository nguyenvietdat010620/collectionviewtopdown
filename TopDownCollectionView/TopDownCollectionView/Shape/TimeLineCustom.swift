//
//  TimeLineCustom.swift
//  TopDownCollectionView
//
//  Created by Nguyen Viet Dat on 08/12/2023.
//

import Foundation
import UIKit

class TimeLineCustom: UICollectionViewLayout {
    var contentSize:CGSize!
    let attributeArray = NSMutableDictionary()
    var numberItem: Int{
        return (collectionView?.numberOfItems(inSection: 0))!
    }
    
    var itemHeight: CGFloat{
        return (collectionView?.frame.height)! / 2 - 20
    }
    var itemWidth:CGFloat{
        return 200
    }
    
    override func prepare() {
        let line = CAShapeLayer()
        let aPath = UIBezierPath()
        var circlePath = UIBezierPath()
        
        aPath.lineWidth = 5
        aPath.stroke()
        aPath.move(to: CGPoint(x:0.0, y: CGFloat((collectionView?.frame.height)!) / 2 + 10.0))
        aPath.addLine(to: CGPoint(x:10.0 + CGFloat(numberItem) * itemWidth, y: CGFloat((collectionView?.frame.height)!) / 2 + 10.0))
        line.path = aPath.cgPath
        line.strokeColor = UIColor.red.cgColor
        UIColor.red.set()
        collectionView?.layer.addSublayer(line)
        for i in 0 ... numberItem - 1 {
            let circle = CAShapeLayer()
            let countLabel = UILabel()
            if((i + 1 ) % 2 == 0){
                var point = CGPoint(x: 38 + itemWidth * CGFloat(i), y: (collectionView?.frame.height)! / 2 + 10.0)
                circlePath = UIBezierPath(arcCenter: point, radius: 25, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: false)
                
                countLabel.text = "\(i + 1 )"
                countLabel.bounds = CGRectMake(38 + itemWidth * CGFloat(i), (collectionView?.frame.height)! / 2 + 10.0, 52, 52)
                countLabel.center = CGPointMake(38 + itemWidth * CGFloat(i), (collectionView?.frame.height)! / 2 + 10.0)
               
            }
            else{
                var point = CGPoint(x: 38 + itemWidth * CGFloat(i), y: (collectionView?.frame.height)! / 2 + 10.0)
                circlePath = UIBezierPath(arcCenter: point, radius: 25, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: false)
                
                countLabel.text = "\(i + 1 )"
                countLabel.bounds = CGRectMake(38 + itemWidth * CGFloat(i), (collectionView?.frame.height)! / 2 + 10.0, 52, 52)
                countLabel.center = CGPointMake(38 + itemWidth * CGFloat(i), (collectionView?.frame.height)! / 2 + 10.0)
            }
            
            circle.path = circlePath.cgPath
            circle.strokeColor = UIColor.red.cgColor
            circle.lineCap = .round
            circle.fillColor = UIColor.red.cgColor
            
            countLabel.layer.cornerRadius = 52 / 2
            countLabel.layer.borderWidth = 6.0
            countLabel.textColor = UIColor.white
            countLabel.layer.backgroundColor = UIColor.clear.cgColor
            countLabel.layer.borderColor = UIColor.green.cgColor
            countLabel.textAlignment = .center
            collectionView?.layer.addSublayer(circle)
            collectionView!.addSubview(countLabel)
            collectionView?.bringSubviewToFront(countLabel)
        }
       
        
        for i in 0 ... numberItem - 1 {
            var tempX : CGFloat = 10.0
            var tempY : CGFloat = 20.0
            let indexPath = NSIndexPath(item: i, section: 0)
            
            if((i + 1 ) % 2 == 0){
                tempX = 10 + itemWidth * CGFloat(i)
                tempY = 20.0
            }
            else{
                tempX = 10 + itemWidth * CGFloat(i)
                tempY = (collectionView?.frame.height)! / 2 + 20.0
            }
            
            let attributes = UICollectionViewLayoutAttributes(forCellWith:indexPath as IndexPath);
            attributes.frame = CGRect(x: tempX, y: tempY, width: itemWidth, height: itemHeight);
            self.attributeArray.setObject(attributes, forKey: indexPath)
        }
        let contentHeight = collectionView?.frame.height
        let contentWidth = itemWidth * CGFloat(numberItem + 1)
        self.contentSize = CGSize(width: contentWidth, height: contentHeight!);
    }
    
    override var collectionViewContentSize: CGSize
    {
        return self.contentSize
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        
        // Duyệt các đối tượng trong attributeArray để tìm ra các cell nằm trong khung nhìn rect
        for attributes  in self.attributeArray {
            if (attributes.value as! UICollectionViewLayoutAttributes).frame.intersects(rect ) {
                layoutAttributes.append((attributes.value as! UICollectionViewLayoutAttributes))
            }
        }
        return layoutAttributes
    }
}
