//
//  ShapeViewController.swift
//  TopDownCollectionView
//
//  Created by Nguyen Viet Dat on 08/12/2023.
//

import UIKit
struct timeLine{
    var color: String
    var content: String
}
class ShapeViewController: UIViewController {
    var listData = [
        ["color" : "red",
         "content": "asdasdasd"
        ],
        ["color" : "red",
         "content": "asdasdasd"
        ],
        ["color" : "red",
         "content": "asdasdasd"
        ],
        ["color" : "red",
         "content": "asdasdasd"
        ],
        ["color" : "red",
         "content": "asdasdasd"
        ]
    ]
    @IBOutlet weak var shapeCollectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "ShapeCollectionViewCell", bundle: nil)
        shapeCollectionView.register(nib, forCellWithReuseIdentifier: "ShapeCollectionViewCell")
        shapeCollectionView.dataSource = self
        shapeCollectionView.delegate = self
        
        let layout =  TimeLineCustom()
        shapeCollectionView.collectionViewLayout = layout
//        shapeCollectionView.backgroundView =
//        setupLine()
        // Do any additional setup after loading the view.
    }
    
//    func setupLine(){
//        let line = CAShapeLayer()
//        let aPath = UIBezierPath()
//        
//        aPath.lineWidth = 100
//        aPath.stroke()
//        aPath.move(to: CGPoint(x:10.0, y:shapeCollectionView.frame.height / 2 + 10))
//        aPath.addLine(to: CGPoint(x: 10 + listData.count * 100, y: Int(shapeCollectionView.frame.height) / 2 + 10))
//        
//        line.path = aPath.cgPath
//        line.strokeColor = UIColor.red.cgColor
//        // Keep using the method addLine until you get to the one where about to close the path
////        aPath.close()
//        // If you want to stroke it with a red color
//        UIColor.red.set()
//        
//        
//        shapeCollectionView.layer.addSublayer(line)
//    }
//    
    
    
}

extension ShapeViewController:UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShapeCollectionViewCell", for: indexPath as IndexPath) as! ShapeCollectionViewCell
        if((indexPath.row + 1) % 2 == 0){
//            cell.viewCircle.isHidden = false
//            cell.viewCircle2.isHidden = true
            cell.spaceTop.constant  = 0
        }
        if((indexPath.row + 1) % 2 == 0){
            cell.spaceBot.constant  = 0
//            cell.viewCircle2.isHidden = false
//            cell.viewCircle.isHidden = true
        }
        return cell
    }
}


