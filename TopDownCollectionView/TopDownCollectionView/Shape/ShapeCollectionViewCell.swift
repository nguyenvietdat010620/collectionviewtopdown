//
//  ShapeCollectionViewCell.swift
//  TopDownCollectionView
//
//  Created by Nguyen Viet Dat on 08/12/2023.
//

import UIKit

class ShapeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewCircle2: UIView!
    @IBOutlet weak var viewCircle: UIView!
    @IBOutlet weak var spaceBot: NSLayoutConstraint!
    @IBOutlet weak var spaceTop: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
