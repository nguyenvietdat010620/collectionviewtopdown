//
//  SocketViewController.swift
//  TopDownCollectionView
//
//  Created by Nguyen Viet Dat on 07/12/2023.
//

import UIKit

class SocketViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    var mSocket = SocketConfig.shared.getSocket()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        doanyThing()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func action(_ sender: Any) {
        mSocket.emit("counter")
    }
    
    func doanyThing() {
        SocketConfig.shared.establishConnection()
        
        mSocket.on("counter") { data, ack in
            print("count",data, ack)
            self.label.text = "\(data)"
        }
        }
    
}
