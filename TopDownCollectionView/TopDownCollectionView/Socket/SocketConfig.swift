//
//  SocketConfig.swift
//  TopDownCollectionView
//
//  Created by Nguyen Viet Dat on 07/12/2023.
//

import Foundation
import SocketIO

class SocketConfig: NSObject {
    static let shared: SocketConfig = SocketConfig()
    let socket = SocketManager(socketURL: URL(string: "http://localhost:3000")!, config: [.log(true), .compress])
        var mSocket: SocketIOClient!

        override init() {
            super.init()
            mSocket = socket.defaultSocket
        }

        func getSocket() -> SocketIOClient {
            return mSocket
        }

        func establishConnection() {
            mSocket.connect()
        }

        func closeConnection() {
            mSocket.disconnect()
        }
    
    
}
